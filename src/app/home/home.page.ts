import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { faMapMarker } from '@fortawesome/free-solid-svg-icons';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Device } from '@ionic-native/device/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  faMapMarker = faMapMarker;

  constructor(
    private httpClient: HttpClient,
    private geolocation: Geolocation,
    public alertController: AlertController,
    private router: Router,
    private device: Device,
    private diagnostic: Diagnostic
  ) { }

  reportPothole() {
    this.diagnostic.isLocationAuthorized().then((authorized) => {
      if (authorized) {
        this.makeReport();
      }
      else {
        this.diagnostic.requestLocationAuthorization().then((isAvailable) => {
          if (isAvailable == this.diagnostic.permissionStatus.GRANTED) {
            this.makeReport();
          }
          else {
            this.presentAlert('Lo sentimos', this.presentAlert('Lo Sentimos', 'Usted debe proporcionar permisos de geolocalización para poder reportar el bache.'))
          }
        }).catch((error) => {
          this.presentAlert('Error', error)
        });
      }
    });
  }

  private makeReport() {
    this.geolocation.getCurrentPosition({ timeout: 10000, enableHighAccuracy: true, maximumAge: 3600 }).then((resp) => {
      let lat = resp.coords.latitude;
      let lng = resp.coords.longitude;
      let additional_data = JSON.stringify({
        platform: this.device.platform,
        model: this.device.model,
        os_version: this.device.version,
        manufacturer: this.device.manufacturer,
        serial: this.device.serial
      });

      this.httpClient.post('http://bache.daxslab.com/report/create', {
        "lat": lat,
        "lng": lng,
        "uuid": this.device.uuid,
        "additional_data": additional_data
      }).subscribe((response: any) => {
        switch (response.status) {
          case 200:
            this.presentAlert('Reporte Creado', 'Su reporte de bache fue enviado satisfactoriamente.');
            break;
          case 400:
            this.presentAlert('Error', 'Ocurrio un error en el envío de sus datos.');
            break;
          case 403:
            if (response.errorCode == 5){
              this.presentAlert('Lo sentimos', 'Usted no puede reportar el mismo bache dos veces.');
            }else{
              this.presentAlert('Lo sentimos', 'Usted no tiene permitido realizar un reporte');
            }
        }
      }, error => {
        this.presentAlert('Ups!!!', 'Ocurrió un error en el envío de su reporte, por favor intente más tarde.');
        console.log(error);
      });


    }).catch((error) => {
      if (error.code == 1)
        this.presentAlert('Lo Sentimos', 'Usted debe proporcionar permisos de geolocalización para poder reportar el bache.')
      this.presentAlert('Lo Sentimos', error.message);
      console.log('Error getting location', error);
    });
  }

  async presentAlert(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  showMap() {
    this.router.navigate(['map']);
  }

}

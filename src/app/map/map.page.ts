import { Component } from '@angular/core';
import { Map, latLng, tileLayer, Layer, marker, icon } from 'leaflet';
import { HttpClient } from '@angular/common/http';

@Component({
    selector: 'app-map',
    templateUrl: 'map.page.html',
    styleUrls: ['map.page.scss'],
})
export class MapPage {

    map: Map;

    constructor(private httpClient: HttpClient) { }

    ionViewDidEnter() { this.leafletMap(); }

    leafletMap() {
        // In setView add latLng and zoom

        this.map = new Map('mapId', {
            minZoom: 14,
            maxZoom: 18
        }).setView([22.14957, -80.44662], 14);
        tileLayer('assets/cfgMap/{z}/{x}/{y}.jpg', {
            attribution: 'bache.cu © Daxslab',
        }).addTo(this.map);

        let dot = icon({
            iconUrl: 'assets/redDot1.png',
            iconSize: [7, 7], // size of the icon
            // shadowSize: [50, 64], // size of the shadow
            // iconAnchor: [22, 94], // point of the icon which will correspond to marker's 
            // shadowAnchor: [4, 62],  // the same for the shadow
            // popupAnchor: [-3, -76] // point from which the popup should open relative..
        });

        // marker([22.14957, -80.44662], { icon: dot }).addTo(this.map);

        this.httpClient.get('http://bache.daxslab.com/pothole/get-trusties')
            .subscribe((response: any) => {
                if (response.code == 200) {
                    response.data.forEach(pothole => {
                        let location = JSON.parse(pothole.location);
                        let lat = location.geometry.coordinates[0];
                        let lng = location.geometry.coordinates[1];
                        console.log(lat);
                        console.log(lng);
                        marker([lat, lng], { icon: dot }).addTo(this.map);
                    });
                }
                console.log(response.message);
            }, error => {
                console.log(error);
            })
    }

    /** Remove map when we have multiple map object */
    ionViewWillLeave() {
        this.map.remove();
    }

}